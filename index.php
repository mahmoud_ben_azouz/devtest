<!DOCTYPE html>
<html data-wf-page="5cd0a4dfc94507558498109b" data-wf-site="5cc08428d2c73015d3b08896">
<head>
  <meta charset="utf-8">
  <title>Login</title>
  <meta content="Login" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/platform.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
  <style>
    .w-select {
      background-image: none;
      background-image: none;
      border-radius: 0px; !important
    }

    #popup{
      padding: 18px
    }

    #popup p, #popup p.more{
      line-height: inherit
    }
  </style>
</head>
<body>
  <div class="mobile">
    <div class="logoCont"><h1 style="text-align:left;display:inline-block"><img src="images/logo.svg" style="width:32px;margin-right:4px" alt="">Concorsi</h1></div>
    <h4>Attualmente il portale non è disponibile da mobile, visita nuovamente questa pagina da desktop</h4>
  </div>
  <div id="overlay">
    <img src="images/closeWhite.svg" width="24" height="24" id="close" alt="" class="close">
    <div id="popup">
      <p id="message">
      </p>
    </div>
  </div>
  <div class="_50 full-page">
    <div class="wrapper">
      <div class="search-filter-bar login w-clearfix">
        <h1 style="text-align:left;display:inline-block"><img src="images/logo.svg" style="width:32px;margin-right:4px" alt="">Concorsi</h1>
        <h4>Bentornato, accedi al tuo account per procedere</h4>
        <form id="email-form" name="email-form" data-name="Email Form" method="POST" action="">
        <div class="search-card w-form form-login">
          <div class="text-field-container no-shadow"><label for="Ricerca-card" class="label-field">Mail</label><input type="text" class="search-field-contain w-input" maxlength="256" name="username" placeholder="Es: 1243" id="Ricerca-card-2"></div>
          <div class="text-field-container no-shadow"><label for="Ricerca-card-3" class="label-field">Password</label><input type="password" class="search-field-contain w-input" maxlength="256" name="password" id="password" placeholder="**********"></div>
        </div>
        <span id="error">
          <?php echo $error.(strlen($error) ? "<br>" : ""); ?>
        </span>
          <a href="reset-psw.php" class="button text-row no-margin w-button">Password dimenticata?</a>
          <a href="adduser.php" class="button text-row no-margin w-button button-left">Nuovo utente?</a>
        </div><input type="submit" value=" Accedi " name="submit" class="button login w-button"></div>
      </form>
    </div>
  </div>
  <div class="_50 full-page img"></div>
  <script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <script>
    function submitConfirm()
    {
      if(document.getElementById("code").value!="")
      {
        if(document.getElementById("code").value.length!=5)
        {
          alert("Inserire il codice corretto");
          return;
        }
        $.ajax({
          url:"confirmOTP.php",
          method:'POST',
          data:{otp:document.getElementById("code").value},
          success:function(data)
          {
            if(data=="1")
            {
              if(p)
                window.location.href="concorsoDetail.php?id="+p;
              else
                window.location.href="dashboard.php";
            }
            else
              $("#message").fadeOut(400,function(){
                $("#message").removeClass("more");
                $("#message").text("Codice OTP errato");
                $("#message").fadeIn();
              });
          }
        });
      }
      else{
        alert("Inserire il codice OTP ricevuto");
        return;
      }
    }

    $(document).ready(function(){
      $("#close").on("click", function(){
        $("#overlay").fadeOut();
      })
    })
  </script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>